package com.example.demo.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entidades.Animales;

@Repository
public interface IAnimalRepository extends CrudRepository<Animales, Long>{

}
