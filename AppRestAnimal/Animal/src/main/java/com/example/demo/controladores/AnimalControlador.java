package com.example.demo.controladores;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entidades.Animales;
import com.example.demo.repositorios.IAnimalRepository;

@Controller
@CrossOrigin
@RequestMapping("animales") // se le asigna un nombre al controlado//
public class AnimalControlador {

	// sobreescriendo el repository para la manipulacion de datos de la base //
		@Autowired
		IAnimalRepository ranimal;

		// Metodo para listar y mostrar el json en la tabla

		// listando los campos de la tabla animales //
		@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		@CrossOrigin
		public List<Animales> getAll() {
			return (List<Animales>) ranimal.findAll();
		}

		// metodo para guardar el registro de la tabla animales//
		@PutMapping(value = "save", produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		@CrossOrigin
		public HashMap<String, String> save(@RequestParam Long clave, @RequestParam String nombre,
				@RequestParam String descripcion, @RequestParam String tipo, @RequestParam Date fecha) {
			
			Animales an = new Animales();
			//creando objeto de la clase animales//
			clave = ranimal.count() + 5;
			
			an = new Animales(clave, nombre, descripcion, tipo, fecha);

			HashMap<String, String> jsonReturn = new HashMap<>();

			try {
				// guardando el objeto de la clase animales//
				ranimal.save(an);

				// Mensajes deConfirmacion //
				jsonReturn.put("Estado", "OK");
				jsonReturn.put("Mensaje", "Registro guardado");

				// Retornando_Mensaje //
				return jsonReturn;
			} catch (Exception e) {

				// Mensajes_De_Confirmacion //
				jsonReturn.put("Estado", "Error");
				jsonReturn.put("Mensaje", "Registro no guardado");

				// Retornando Mensaje //
				return jsonReturn;
			}
		}

		// metodo para actualizar los registros//
		@PutMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		@CrossOrigin
		public HashMap<String, String> update(@RequestParam Long clave, @RequestParam String nombre,
				@RequestParam String descripcion, @RequestParam String tipo, @RequestParam Date fecha) {
			// creando objeto de la clase animales//
			Animales an = new Animales();
			
			an = new Animales(clave, nombre, descripcion, tipo, fecha);

			HashMap<String, String> jsonReturn = new HashMap<>();

			try {
				// guardando el objeto de la clase animales//
				ranimal.save(an);

				// Mensajes deConfirmacion //
				jsonReturn.put("Estado", "OK");
				jsonReturn.put("Mensaje", "Registro guardado");

				// Retornando_Mensaje //
				return jsonReturn;
			} catch (Exception e) {

				// Mensajes_De_Confirmacion //
				jsonReturn.put("Estado", "Error");
				jsonReturn.put("Mensaje", "Registro no guardado");

				// Retornando Mensaje //
				return jsonReturn;

			}
		}

		// metodo para buscar por clave//
		@PostMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		public Animales getMethodName(@PathVariable Long id) {
			return ranimal.findById(id).get();
		}
		//metodo para eliminar los registros//
		@DeleteMapping(value ="{id}",produces = MediaType.APPLICATION_JSON_VALUE)
		@ResponseBody
		public Boolean delete(@PathVariable Long id) {
			Animales an= ranimal.findById(id).get();
			ranimal.delete(an);
			return true;
		}
	}

