let an = {
    id:0
}
// funcion para recolectar el id//
function setId(id){
an.id =id
    }
$(document).ready(inicio);
// Se inicializa la funcion para cargar los datos de la tabla de animales //
function inicio() {
 /* Cargar los datos de la tabla de animal */
cargarDatos();
/*
 * Evento click con el boton guardar para el nuevo registro de la tabla
 * animal
 */
$("#btnGuardar").click(guardar);
/* Evento click con el boton eliminar registro de animal */
$("#btnEliminar").click(function (){
    eliminar(an.id)
});
// Evento click para modificar del animal //
$("#btnActualizar").click(modificar);
$("#btnCancelar").click(reset);
}
// funcion para resetar el formulario con los inputs//
function reset(){
$("#nombre").val(null);
$("#descripcion").val(null);

$("#nombre2").val(null);
$("#descripcion2").val(null);
}
// metodo para cargar los datos con una peticion ajax en la tabla de animales//
function cargarDatos() {
$.ajax({
    url: "http://localhost:8080/animales/",
    method: "Get",
    data:null,
    success: function (response){
        $("#datos").html("");
        
    for (let i = 0 ; i < response.length; i++) {
            $("#datos").append(
            "<tr>" +
                "<td>"+ response[i].clave + "</td>"+
                "<td>"+ response[i].nombre + "</td>"+
                "<td>"+ response[i].descripcion +" </td>"+
                "<td>"+ response[i].tipo + "</td>"+
                "<td>"+ response[i].fechaCreacion + "</td>"+
                "<td>"+
                "<button onclick='cargarRegistro("+ response[i].clave +
                ")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><i class='fas fa-edit'></i>Editar</button>" +
                "<button onclick='setId(" + response[i].clave +
                ")' type='button' class='btn btn-danger ml-3 mt-1' style='color: black' data-toggle='modal' data-target='#eliminar'><i class='fas fa-trash-alt'></i> Eliminar</button>" +
                "</td>" +
                "</tr>"
            )
        }
    },
    error: function (response) {
        alert("Error: " + response);
    }
});
}
// funcion para guardar un nuevo registro atraves de una peticion ajax//
function guardar(response){
var fecha = new Date();
$.ajax({
    url:"http://localhost:8080/animales/save",
    method:"Put",
    data:{
        clave: null,
        nombre:$("#nombre").val(),
        descripcion:$("#descripcion").val(),
        tipo:$("#tipo").val(),
        fecha: fecha
        
    },
    success:function(){
        cargarDatos();
        reset();
    },
    error: function (response) {
        alert("Error en la peticion: " + response)
    }
})
}
//funcion para eliminar el registro//
function eliminar(id){
$.ajax({
    url: "http://localhost:8080/animales/" +id,
    method: "Delete",
    success: function (){
        cargarDatos();
    },
    error: function (response) {
        alert("Error en la peticion: " + response)
    }
})
}
//funcion cargar el registro mediante el id//
function cargarRegistro(id){
$.ajax({
    url:  "http://localhost:8080/animales/" +id,
    method: "Post",
    success: function (response){
        $("#clave2").val(response.clave)
        $("#nombre2").val(response.nombre)
        $("#descripcion2").val(response.descripcion)
        $("#tipo2").val(response.tipo)	
    },
    error: function (response) {
        alert("Error en la peticion: " + response)
    }	
})
}
//function para modificar el registro//
function modificar() {
var id = $("#clave2").val();
var fecha = new Date();
$.ajax({
    url: "http://localhost:8080/animales/update/",
    method: "Put",
    data: {
        clave: id,
        nombre: $("#nombre2").val(),
        descripcion: $("#descripcion2").val(),
        tipo: $("#tipo2").val(),
        fecha: fecha
    },
    success: function (response) {
        cargarDatos();
        reset();
    },
    error: function (response) {
        alert("Error en la peticion: " + response);
        console.log(id);
    }
});
}