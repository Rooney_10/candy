let maquina = {
    id: 0
}

function setIdMaquina(id) {
    maquina.codigo = id
}

//FUncion ready
$(document).ready(inicio);

//FUNCION INICIO
function inicio() {
    cargarDatos();
    $("#btnGuardar").click(guardar);
    $("#btnEliminar").click(function () {
        eliminar(maquina.codigo)
    });
    $("#btnActualizar").click(modificar);
    $("#btnCancelar").click(reset);
}
//Funcion para resetear los datos(limpear)
function reset() {
    $("#nombre").val(null);
    $("#descripcion").val(null);

    $("#nombre2").val(null);
    $("#descripcion2").val(null);
}

//CARGANDO DATOS A TABLA
//Cargar datos con una peticion ajax
function cargarDatos() {
    $.ajax({
        url: "http://localhost:8081/maquinas/all",
        method: "Get",
        data: null,
        success: function (response) {
            $("#datos").html("");
            //for para insertar datos en la tabla
            
            for (let i = 0; i < response.length; i++) {
                $("#datos").append(
                    "<tr>" +
                    "<td><strong>" + response[i].codigo + "</strong></td>" +
                    "<td><strong>" + response[i].nombre + "</strong></td>" +
                    "<td><strong>" + response[i].descripcion + "</strong></td>" +
                    "<td><strong>" + response[i].tipo + "</strong></td>" +
                    "<td>" +
                    "<button onclick='cargarRegistro(" + response[i].codigo +
                    ")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><strong>Editar</strong></button>" +
                    
                    "<button onclick='setIdMaquina(" + response[i].codigo +
                    ");' type='button' class='btn btn-danger ml-3 mt-1' style='color: black' data-toggle='modal' data-target='#eliminar'><strong>Eliminar</strong></button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        error: function (response) {
            alert("Error: " + response);
        }
    });
}
//Funcion para guardar o actualizar un registro
function guardar(response) {
    $.ajax({
        url: "http://localhost:8081/maquinas/saveOrUpdate",
        method: "Post",
        data: {
            id: null,
            nombre: $("#nombre").val(),
            descripcion: $("#descripcion").val(),
            tipo: $("#tipo").val()
        },
        success: function () {
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}
//Funcion para eliminar un registro
function eliminar(id) {
    $.ajax({
        url: "http://localhost:8081/maquinas/" + id,
        method: "Delete",
        success: function () {
            cargarDatos();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}
//Funcion para cargar los registros
function cargarRegistro(id) {
    $.ajax({
        url: "http://localhost:8081/maquinas/" + id,
        method: "Post",
        success: function (response) {
            $("#id2").val(response.codigo)
            $("#nombre2").val(response.nombre)
            $("#descripcion2").val(response.descripcion)
            $("#tipo2").val(response.tipo)
        },
        error: function (response) {
            alert("Error en la peticion " + response);
        }
    })
}
//Funcion para modificar registros
function modificar() {
    var id = $("#id2").val();
    $.ajax({
        url: "http://localhost:8081/maquinas/saveOrUpdate/",
        method: "put",
        data: {
            id: id,
            nombre: $("#nombre2").val(),
            descripcion: $("#descripcion2").val(),
            tipo: $("#tipo2").val()
        },
        success: function (response) {
            console.log(response);
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response);
            console.log(id);
        }
    });
}