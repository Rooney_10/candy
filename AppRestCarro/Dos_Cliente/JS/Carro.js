let carro = {
    id: 0
}

function setIdCarro(id) {
    carro.id = id
}

$(document).ready(inicio);

//FUNCION INICIO
function inicio() {
    cargarDatos();
    $("#btnGuardar").click(guardar);
    $("#btnEliminar").click(function () {
        eliminar(carro.id)
    });
    $("#btnActualizar").click(modificar);
    $("#btnCancelar").click(reset);
}

function reset() {
    $("#modelo").val(null);
    $("#marca").val(null);

    $("#modelo2").val(null);
    $("#marca2").val(null);
}

//CARGANDO DATOS A TABLA ESPECIALIDADES
function cargarDatos() {
    $.ajax({
        url: "http://localhost:8080/carros/all",
        method: "Get",
        data: null,
        success: function (response) {
            $("#datos").html("");

            for (let i = 0; i < response.length; i++) {
                $("#datos").append(
                    "<tr>" +
                    "<td><strong>" + response[i].id + "</strong></td>" +
                    "<td><strong>" + response[i].modelo + "</strong></td>" +
                    "<td><strong>" + response[i].marca + "</strong></td>" +
                    "<td><strong>" + response[i].estado + "</strong></td>" +
                    "<td>" +
                    "<button onclick='cargarRegistro(" + response[i].id +
                    ")'type='button' class='btn btn-warning ml-3 mt-1' data-toggle='modal' data-target='#editar'><i class='fas fa-edit'></i> <strong>Editar</strong></button>" +
                    "<button onclick='setIdCarro(" + response[i].id +
                    ");' type='button' class='btn btn-danger ml-3 mt-1' style='color: black' data-toggle='modal' data-target='#eliminar'><i class='fas fa-trash-alt'></i> <strong>Eliminar</strong></button>" +
                    "</td>" +
                    "</tr>"
                )
            }
        },
        error: function (response) {
            alert("Error: " + response);
        }
    });
}

function guardar(response) {
    $.ajax({
        url: "http://localhost:8080/carros/saveOrUpdate",
        method: "Put",
        data: {
            id: null,
            modelo: $("#modelo").val(),
            marca: $("#marca").val(),
            estado: $("#estado").val()
        },
        success: function () {
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function eliminar(id) {
    $.ajax({
        url: "http://localhost:8080/carros/" + id,
        method: "Delete",
        success: function () {
            cargarDatos();
        },
        error: function (response) {
            alert("Error en la peticion: " + response)
        }
    })
}

function cargarRegistro(id) {
    $.ajax({
        url: "http://localhost:8080/carros/" + id,
        method: "Post",
        success: function (response) {
            $("#id2").val(response.id)
            $("#modelo2").val(response.modelo)
            $("#marca2").val(response.marca)
            $("#estado2").val(response.estado)
        },
        error: function (response) {
            alert("Error en la peticion " + response);
        }
    })
}

function modificar() {
    var id = $("#id2").val();
    $.ajax({
        url: "http://localhost:8080/carros/saveOrUpdate/",
        method: "put",
        data: {
            id: id,
            modelo: $("#modelo2").val(),
            marca: $("#marca2").val(),
            estado: $("#estado2").val()
        },
        success: function (response) {
            console.log(response);
            cargarDatos();
            reset();
        },
        error: function (response) {
            alert("Error en la peticion: " + response);
            console.log(id);
        }
    });
}